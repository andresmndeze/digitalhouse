let categoryValidation = false;
let productValidation = false;
let priceValidation = false;
let brandValidation = false;
let avatarValidation = true;
const p = document.getElementsByTagName('p');
const btn = document.getElementsByTagName('btn');
const form = document.getElementsByTagName('form');
const input = document.getElementsByTagName('input');
let category = document.querySelector('#category_id');


input['name'].onblur = function(){
	if((input['name'].value).length <= 3){
		p["name_error"].innerHTML = 'Debe ingresar un nombre de producto correcto';
		p["name_error"].style.color = "red";
		productValidation = false;
		verify();
	}else {
		p["name_error"].innerHTML = '';
		productValidation = true;
		verify();
	}
}

input['price'].onblur = function(){
	if (input['price'].type !== "number" || input['price'].value === ""){
		p["price_error"].innerHTML = 'Debe ingresar un formato correcto';
		p["price_error"].style.color = "red";
		priceValidation = false;
		verify();
	} else {
		p["price_error"].innerHTML = '';
		priceValidation = true;
		verify();
	}
}

input['brand'].onblur = function(){
	if(input['brand'].value === ""){
		p["brand_error"].innerHTML = 'Debes asignar una marca al producto';
		p["brand_error"].style.color = "red";
		brandValidation = false;
		verify();
	} else {
		p["brand_error"].innerHTML = '';
		brandValidation = true;
		verify();
		}
}

function categoryCheck() {
	categoryValidation = false;

	if (category.value > 0) {
		p["category_name"].innerHTML= "";
		categoryValidation = true;
		verify();
	} else {
		p["category_name"].innerHTML = "Seleccione una categoria correcta";
		p["category_name"].style.color = "red";
		categoryValidation = false;
		verify();
	}
}


function avatarfunc(){
	if (input["avatar"] !== ""){
		let avatar_size = input['avatar']['files'][0]['size'];
		let avatar_type = input['avatar']['files'][0]['type'];
		if (avatar_size > (5000*1024)){
			p["avatar_error"].innerHTML = "La imagen no puede superar los 5mb";
			p["avatar_error"].style.color = "red";
			avatarValidation = false;
		} else {
			if (avatar_type == "image/jpeg" && avatar_type == "image/webp" && avatar_type == "image/png" && avatar_type == "image/jpg"){
				p["avatar_error"].innerHTML= "No es un tipo de archivo jpg, png, jpeg o webp";
				p["avatar_error"].style.color = "red";
				avatarValidation = false;
			} else {
				p["avatar_error"].innerHTML = "Esta bien el tamaño y el tipo de archivo de la imagen";
				p["avatar_error"].style.color = "green";
				avatarValidation = true;
			}
		}
	}	else {
		p["avatar"].innerHTML = "";
		avatarValidation = true;
	}
}

function verify(){
	if (productValidation
		&& categoryValidation
		&& brandValidation
		&& priceValidation
		&& avatarValidation)
	{
		document.getElementById("submit").disabled = false;
	} else {
		document.getElementById("submit").disabled = true;
	}
}
