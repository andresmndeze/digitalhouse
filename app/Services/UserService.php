<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\User;
use Exception;

class UserService
{

    /**
     * getCategories
     * get all categories
     * @return object
     */
    public function getAllUsers()
    {
        return User::whereNull('deleted_at')->paginate(10);
    }

    /**
     * getUserById
     * @param  integer user id to find
     * @return object Return an array with the request
     */
    public function getUserById($user_id)
    {
        return User::find($user_id);
    }

    /**
     * getUserById
     * @return void
     */
    public function modifyUser(Request $request)
    {
        $user = $this->getUserById($request->input('id'));
        $user->name     = $request->input('name');
        $user->email    = $request->input('email');
        $user->is_admin = $request->input('is_admin');
        $user->save();
    }

    public function deleteUser($user_id)
    {
        $user = $this->getUserById($user_id);
        $user->deleted_at = date("Y-m-d");
        $user->save();
    }


}
