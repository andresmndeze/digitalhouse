<?php

namespace App\Services;

use App\Models\Product;
use App\Services\CategoryService;
use Illuminate\Support\Facades\DB;
use Exception;

class ProductService
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * getAllProducts
     * get all products
     * @return object
     */
    public function getAllProducts()
    {
        return Product::whereNull('delete_at')->paginate(10);
    }

    public function getProductById($product_id)
    {
        return Product::find($product_id);
    }

    public function getProductsByCategory($category)
    {
        return $category->products;
    }

    public function getHomeProducts()
    {
        return Product::whereNull('delete_at')->limit(4)->get();
    }

    public function editProductCategory($product, $category)
    {
        $product->categories()->sync($category->id);
    }

    public function editProduct($product, $request)
    {
        $product->name        = $request->input('name');
        $product->description = $request->input('description');
        $product->price       = $request->input('price');
        $product->brand       = $request->input('brand');

        $this->modifyProductAvatar($product, $request);

        $product->save();

    }

    public function getProductsForPage()
    {
        return Product::whereNull('delete_at')->paginate(10);
    }

    public function deleteProduct($product)
    {
        $product->delete_at = date("Y-m-d ");
        $product->save();
    }

    public function modifyProductAvatar($product, $request)
    {
        if ( $request->file('avatar') ){
            $file   = $request->file('avatar');
            $name   = str_replace(" ", "_", $product->name) . "." . $file->extension();
            $folder = $request->input('category_name');
            $path   = $file->storeAs($folder,$name);
            $path   = "/" . "storage/images/" . $request->input('category_name') . "/$name";
            $product->image = $path;
        }
    }

    public function createNewProduct($request)
    {
        $product              = new Product;
        $product->name        = $request->input('name');
        $product->description = $request->input('description');
        $product->price       = $request->input('price');
        $product->brand       = $request->input('brand');
        if(is_null($request->input('category_id')))
        {
            $category = $this->categoryService->createNewCategory($request->input('category_name'));
        } else {
            $category= $this->categoryService->getCategoryById($request->input('category_id'));
        }
        $this->modifyProductAvatar($product, $request);
        $product->save();
        $this->editProductCategory($product, $category);
        return $product;
    }

    public function searchProduct($request)
    {
        $search = "%".$request->input('search')."%";
        $products = DB::table('products')->where('name','like',$search)->orWhere('description','like',$search)->orWhere('brand','like',$search)->orderBy('name')->get();
        $search = $request->input('search');
        return array($search, $products);
    }

}
