<?php

namespace App\Services;

use App\Models\Category;
use Exception;

class CategoryService
{

    /**
     * getCategories
     * get all categories
     * @return object
     */
    public function getCategories()
    {
        return Category::all();
    }

    public function getCategoryByName($category_name)
    {
        return Category::where('name',$category_name)->get();
    }

    public function createNewCategory($category_name)
    {
        $category= new Category(['name' => $category_name]);
        $category->save();
        return $category;
    }

    public function getCategoryById($category_id)
    {
        return Category::find($category_id);
    }

}
