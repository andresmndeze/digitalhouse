<?php

namespace App\Services;

use App\Models\Cart;

class CartService
{
    public function getAllCarts()
    {
        return Cart::all();
    }

    public function getCartProducts($cart)
    {
        return $cart->products()->get();
    }

    public function  getCartProductByProductId($cart, $product_id)
    {
        return $cart->products()
            ->where('cart_id',$cart->id)
            ->where('product_id', $product_id)
            ->get();

    }

    public function getCartByUserId($user_id)
    {
        return Cart::where([['user_id', $user_id],['purchased', 0]])->get();
    }

    public function addProduct($cart, $product)
    {
        $cart->products()
            ->attach( $product->id ,array('precio'=> $product->price,'quantity' => 1));
    }

    public function removeProduct($cart, $product)
    {
        $cart->products()->detach( $product[0]->id);
    }


    public function increaseCartProductQty($cartProduct)
    {
        $cartProduct->pivot->quantity++;
        $cartProduct->pivot->save();
    }

    public function decreaseCartProductQty($cartProduct)
    {
        $cartProduct->pivot->quantity--;
        $cartProduct->pivot->save();
    }

    public function createNewCart($user)
    {
        $cart = New Cart;
        $cart->purchased = 0;
        $cart->user_id = $user->id;
        $cart->total = 0;
        $cart->save();
    }

    public function getLastCart()
    {
        return Cart::orderBy('id','DESC')->first();
    }

    public function setTotalPrice($products, $cart)
    {
        foreach ($products as $product) {
            $cart->total += ($product->price) * ($product->pivot->quantity);
        }
        $cart->save();
    }

    public function getCartPurchased($user_id)
    {
        return Cart::where([['user_id', $user_id],['purchased', 1]])->orderBy('id','desc')->get();
    }

}
