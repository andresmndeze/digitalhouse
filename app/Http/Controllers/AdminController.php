<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Services\ProductService;
use App\Services\CategoryService;
use App\Services\UserService;

class AdminController extends Controller
{
    private $productService;
    private $categoryService;
    private $userService;

	public function __construct(ProductService $productService, CategoryService  $categoryService, UserService $userService)
	{
			$this->middleware('admin');
			$this->categoryService = $categoryService;
			$this->productService = $productService;
			$this->userService = $userService;
	}

	public function addProduct()
	{
		$categories = $this->categoryService->getCategories();
		return view('web.backend.products.addProduct',compact('categories'));
	}

	public function catalog()
	{
		$productos = $this->productService->getAllProducts();
		$categories = $this->categoryService->getCategories();
		return view('web.backend.products.products',compact('productos', 'categories'));
	}

	public static function index()
	{
		$var = 'My Admin page';
		return view('layouts/auth/index',['var'=>$var]);
	}

	public function users()
	{
		$users = $this->userService->getAllUsers();
		return view('web.backend.user.users',compact('users'));
	}

	public function editor($id)
	{
		$product = $this->productService->getProductById($id);
		$categories = $this->categoryService->getCategories();
		return view('web.backend.products.change',compact('product', 'categories'));
	}

	public function editorUser($id)
	{
		$user = $this->userService->getUserById($id);
		return view('web.backend.user.change',compact('user'));
	}

	public function editUser(Request $request)
	{
		$this->userService->modifyUser($request);
		$users = $this->userService->getAllUsers();
		return view('web.backend.user.users',compact('users'));
	}

	public function newCategory()
    {
        $categories = $this->categoryService->getCategories();
        return view('web.backend.category.addCategory',compact('categories'));
    }

    public function addCategory(Request $request){
        $categories = $this->categoryService->getCategories();
        $this->categoryService->createNewCategory($request->input('category_name'));
        return view('web.backend.category.addCategory',compact('categories'));

    }

}
