<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Services\UserService;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

	public function delete($user_id)
	{
        $this->userService->deleteUser($user_id);
		$saved = "The user has been deleted";
		$users = $this->userService->getAllUsers();
		return view('web.backend.user.users',compact('users','saved'));
	}
}
