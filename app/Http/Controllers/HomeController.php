<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;

class HomeController extends Controller
{
    private $productService;
    private $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryService->getCategories();
        $products = $this->productService->getHomeProducts();
        return view('web.frontend.sections.static.index', compact('categories','products' ));
    }

    public function faqs()
    {
        $nombre = $this->categoryService->getCategories();       
        return view('web.frontend.sections.static.faqs',compact('nombre'));
    }
}
