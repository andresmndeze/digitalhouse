<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Resources\CategoryCollection;
use App\Models\Product;
use App\Http\Resources\ProductCollection;
use App\Models\User;
use App\Http\Resources\UserCollection;


class ApiController extends Controller
{
	public static function products()
	{
		return  new ProductCollection(Product::all());
	}

	public static function categories()
	{
		return new CategoryCollection(Category::all());
	}

	public static function categoriesAndProducts()
	{
		$categories = Category::all();
		foreach ($categories as $category) {
			$prodcat[$category->name] = $category->products;
		}
		return new CategoryCollection($categories);
	}

	public static function users()
	{
		return new UserCollection(User::all());
	}


}
