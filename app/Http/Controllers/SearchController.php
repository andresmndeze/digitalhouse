<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Services\ProductService;
use App\Services\CategoryService;


class SearchController extends Controller
{
    private $productService;
    private $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

	public function buscarPost(Request $request)
	{
	    if (strpos($request->input('search'),";")){
			return redirect('/home');
		}

        $categories = $this->categoryService->getCategories();
	    $info = $this->productService->searchProduct($request);
        $search = $info[0];
	    $products = $info[1];
        return view('web.frontend.sections.products.results',compact('categories','products','search'));


	}

}
