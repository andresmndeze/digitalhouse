<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;

class ProductController extends Controller
{
    private $productService;
    private $categoryService;
    protected $dates = ['delete_at'];

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function show($category_name, $product_id)
    {
        $categories = $this->categoryService->getCategories();
        $category = $this->categoryService->getCategoryByName($category_name);
        $product = $this->productService->getProductById($product_id);
        return view('web.frontend.sections.products.productview',compact('categories','category','product'));
    }

    public function edit(Request $request)
    {
        $categories = $this->categoryService->getCategories();
        $categoryExists = false;
        $product = $this->productService->getProductById($request->input('id'));

        foreach ($categories as $category) {
            if(strtolower($category->name) == strtolower($request->input('category'))){
                $categoryExists = true; //la Categoria la categoria existe
                $this->productService->editProductCategory($product, $category);
            }
        }
        if ($categoryExists == false){
            $this->categoryService->createNewCategory($request->input('category'));
            $category = $this->categoryService->getCategoryByName($request->input('category'));
            $this->productService->editProductCategory($product, $category);
        }

        $this->productService->editProduct($product, $request);

        /*Me fijo si se agrego una categoria*/
        $productos = $this->productService->getProductsForPage();
        $saved = "Se han guardado los cambios";
        return view('web.backend.products.products',compact('productos','saved'));
    }

    public function delete(Request $request)
    {
        $product = $this->productService->getProductById( $request->input('id') );
        $this->productService->deleteProduct($product);
        $saved = "Se ha borrado con exito el articulo";
        $productos = $this->productService->getProductsForPage();
        return view('web.backend.products.products',compact('productos','saved'));
    }

    public function add(Request $request)
    {
        $this->productService->createNewProduct($request);
        $categories = $this->categoryService->getCategories();
        return view('web.backend.products.addProduct',compact('categories'));
    }
}
