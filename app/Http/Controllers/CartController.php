<?php

namespace App\Http\Controllers;


use App\Services\CartService;
use App\Services\CategoryService;
use App\Services\ProductService;

class CartController extends Controller
{
    private $cartService;
    private $categoryService;
    private $productService;

    public function __construct(CartService $cartService, CategoryService  $categoryService, ProductService $productService)
    {
        $this->cartService = $cartService;
        $this->categoryService = $categoryService;
        $this->productService = $productService;
    }

    //mostrar carrito

    public function show(){
        if (\Auth::user()){
            $carts = $this->cartService->getAllCarts();
            foreach ($carts as $cart) {
                if ($cart->user_id == \Auth::user()->id && $cart->purchased==0){
                    $productos = $this->cartService->getCartProducts($cart);
                    $categories = $this->categoryService->getCategories();
                    return view('web.frontend.sections.cart.view',compact('productos','categories'));
                }
            }
            $categories = $this->categoryService->getCategories();
            return view('web.frontend.sections.cart.defaultview',compact('categories'));
        } else {
            return view('auth.login');
        }
    }

    //agregar item
    public function add($product_id)
    {
        if (\Auth::user()){
            $product = $this->productService->getProductById($product_id);
            $carts = $this->cartService->getAllCarts();

            /*Busco el carrito del usuario*/
            foreach ( $carts as $cart ) {
                if ( $cart->user_id == \Auth::user()->id && $cart->purchased == 0 ){

                    $cartProduct = $this->cartService->getCartProductByProductId($cart, $product_id);

                    //si el producto no existe en  el carrito lo agrego
                    if($cartProduct == '[]'){
                        $this->cartService->addProduct($cart, $product);
                        $productos = $this->cartService->getCartProducts($cart);
                        $categories = $this->categoryService->getCategories();
                        return view('web.frontend.sections.cart.view',compact('productos','categories'));
                    } else {

                        $this->cartService->increaseCartProductQty($cartProduct[0]);
                        $productos = $this->cartService->getCartProducts($cart);
                        $categories = $this->categoryService->getCategories();
                        return view('web.frontend.sections.cart.view',compact('productos','categories'));
                    }
                }
            }
            /*si no encontro un carrito no comprado*/
            $user = \Auth::user();
            $this->cartService->createNewCart($user);
            $cart = $this->cartService->getLastCart();
            $this->cartService->addProduct($cart, $product);
            $productos = $this->cartService->getCartProducts($cart);
            $categories = $this->categoryService->getCategories();
            return view('web.frontend.sections.cart.view',compact('productos','categories'));
        } else {
            return view('auth.login');
        }
    }

    //quitar  item
    public function quitar($product_id)
    {
        if (\Auth::user()){

            $carts = $this->cartService->getAllCarts();
            $product= $this->productService->getProductById($product_id);
            $categories = $this->categoryService->getCategories();

            foreach ($carts as $cart) {
                if ($cart->user_id == \Auth::user()->id && $cart->purchased == 0){
                    $cartProduct = $this->cartService->getCartProductByProductId($cart, $product_id);
                    if ($cartProduct != '[]'){
                        if($cartProduct[0]->pivot->quantity > 1){
                            $this->cartService->decreaseCartProductQty($cartProduct[0]);
                            $productos = $this->cartService->getCartProducts($cart);
                        } else {
                            $this->cartService->removeProduct($cart, $cartProduct);
                            $productos= $this->cartService->getCartProducts($cart);
                        }
                    }
                }
            }
            if(isset($productos) && $productos != '[]'){
                return view('web.frontend.sections.cart.view',compact('productos','categories'));
            } else {
                return view('web.frontend.sections.cart.defaultview',compact('categories'));
            }
        }
    }

    //actualizar
    public function compra()
    {
        if (\Auth::user()){
            $cart = $this->cartService->getCartByUserId(\Auth::user()->id);
            $categories = $this->categoryService->getCategories();
            if($cart == '[]'){
                return view('web.frontend.sections.cart.defaultview',compact('categories'));
            };
            $cart[0]->purchased=1;
            $products = $this->cartService->getCartProducts($cart[0]);
            $this->cartService->setTotalPrice($products, $cart[0]);
            $purchases = $this->cartService->getCartPurchased(\Auth::user()->id);
            return view('web.frontend.sections.cart.purchases',compact('categories','purchases'));
        } else {
            return view('auth.login');
        }
    }

    public function comprasRealizadas(){
        if (\Auth::user()){
            $purchases = $this->cartService->getCartPurchased(\Auth::user()->id);
            $categories = $this->categoryService->getCategories();     
            return view('web.frontend.sections.cart.purchases',compact('categories','purchases'));
        } else {
            return view('auth.login');
        }
    }
}
