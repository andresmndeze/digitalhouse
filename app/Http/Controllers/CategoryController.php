<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\ProductService;

class CategoryController extends Controller
{
    private $categoryService;
    private $productService;

    public function __construct(CategoryService $categoryService, ProductService $productService)
    {
        $this->categoryService = $categoryService;
        $this->productService = $productService;
    }

    public function index()
	{
	    $categories = $this->categoryService->getCategories();
	    return view('web.frontend.sections.categories.categories',compact('categories'));
	}


  public function show($category_name)
  {
      $category = $this->categoryService->getCategoryByName($category_name);
      $products = $this->productService->getProductsByCategory($category[0]);
      $categories = $this->categoryService->getCategories();
      return view('web.frontend.sections.products.products-category',compact('category','products','categories'));
  }

}
