<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cart;
use App\Models\Category;

class Product extends Model
{
	public function categories()
	{
		return $this->belongsToMany(Category::class,'category_product')->withPivot("category_id");
	}
	public function carts()
	{
		return $this->belongsToMany(Cart::class)->withPivot('precio','quantity')->withTimestamps();
	}
}
