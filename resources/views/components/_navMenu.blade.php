<nav class="navbar  navbar-expand-lg navbar-dark bg-dark" style="width: 100%">
        <a class="navbar-brand" href="#">Menu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                @if( !is_null($categories) )
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Categorias
                        </a>
                        <div class="dropdown-menu navbar-dark bg-dark" aria-labelledby="navbarDropdownMenuLink">
                            @foreach ($categories as $value)
                                <a class="dropdown-item" href="/categories/{{$value->name}}" style="color:#CAC4C3;text-transform: capitalize;">{{$value->name}}</a>
                            @endforeach
                        </div>
                    </li>
                @endif

                @auth()
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Salir</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/cart/compras">Mis carritos</a>
                        </li>
                        @if (Auth::user()->is_admin)
                            <li>
                                <a class="nav-link" href="/admin/addProduct">Agregar Producto</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/admin/catalog">Catalogo</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/admin/users">Administrar Usuarios</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/admin/new-category">Agregar Categoria</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Ingresar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Registrarse</a>
                        </li>
                @endauth
            </ul>
        </div>
    </nav>

