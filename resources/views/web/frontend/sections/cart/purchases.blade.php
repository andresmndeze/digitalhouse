@extends ('web.layouts.app')

@section('title')
{{"Compras Realizadas"}}
@endsection

@section('content')

  	{{"Se realizo la compra con exitos"}}
		@foreach ($purchases as $purchased)
		<div class="table-responsive">
			<h2>{{$purchased->id}}</h2>
			<table class="table .table-hover table-striped table-bordered">
	      <thead class="thead-dark">
	        <tr>
	          <th scope="col">Nombre</th>
	          <th scope="col">Precio</th>
	          <th scope="col">Cantidad</th>
	          <th scope="col">Total</th>
	        </tr>
	      </thead>
				<tbody>
	        @foreach ($purchased->products as $product)
	          <tr>
	            <td>{{ $product->name }}</td>
	            <td id="price">{{ $product->price }}</td>
	            <td>{{$product->pivot->quantity}}</td>
	            <td class="finalPrice">{{ $product->pivot->precio * $product->pivot->quantity }}</td>
	          </tr>
	        @endforeach
	      </tbody>
	    </table>
  	</div>
	@endforeach
@endsection
