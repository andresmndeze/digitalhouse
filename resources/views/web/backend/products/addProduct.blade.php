@extends('web.layouts.app')
@section('title')
	{{'Add Products'}}
@endsection
@section('content')
	<form class="" action="/admin/addProduct" enctype="multipart/form-data" style="display:flex;flex-wrap:wrap;flex-direction:column" method='post'>
		@csrf
		@method('post')

		<div class="form-group row">
			<label for="name" class="col-sm-2 col-form-label">Producto</label>
			<div class="col-sm-10">
				<input id="name" type="text" name="name" class="form-control" value="">
				<p id="name_error" ></p>
			</div>
		</div>

		<div class="form-group row">
			<label for="description" class="col-sm-2 col-form-label">Descripción</label>
			<div class="col-sm-10">
				<input id="description" type="text" name="description" class="form-control" value="">
				<p id="description_error" class="invalid-feedback"></p>
			</div>
		</div>

		<div class="form-group row">
			<label for="avatar" class="col-sm-2 col-form-label">Imagen</label>
			<div class="col-sm-10 custom-file">
				<input id="avatar" type="file" name="avatar" class="form-control-file" value="" onchange="avatarfunc()">
				<p id="avatar_error" class="invalid-feedback"></p>
			</div>
		</div>

		<div class="form-group row">
			<label for="price" class="col-sm-2 col-form-label">Precio</label>
			<div class="col-sm-10">
				<input id="price" type="number" name="price" class="form-control" value="">
				<p id="price_error"></p>
			</div>
		</div>

		<div class="form-group row">
			<label for="brand" class="col-sm-2 col-form-label">Marca</label>
			<div class="col-sm-10">
				<input id="brand" type="text" name="brand" class="form-control" value="">
				<p id="brand_error" class="invalid-feedback"></p>
			</div>
		</div>

		<div class="form-group row">
			<label for="category_id" class="col-sm-2 col-form-label">Elige una categoria</label>
			<div class="col-sm-10">
				<select name="category_id" id="category_id" class="custom-select my-1 mr-sm-2" onchange="categoryCheck()">
					<option selected>Seleccionar...</option>
					@foreach($categories as $category)
						<option value="{{$category->id}}">{{ ucfirst($category->name) }}</option>
					@endforeach
				</select>
				<p id="category_name" class="invalid-feedback"></p>
			</div>
		</div>

		<button id="submit" type="submit" name="submit" value="submit" class="btn btn-primary" disabled>Agregar</button>
			<p id="submit_error" class="invalid-feedback"></p>
		</form>
		<script type="text/javascript"  src="/js/addProducts.js">

		</script>
@endsection
