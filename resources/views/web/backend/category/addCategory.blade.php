@extends('web.layouts.app')
@section('title')
    {{'Add Products'}}
@endsection
@section('content')
    <div class="col-9 align-self-center">
        <form class="" action="/admin/addCategory" enctype="multipart/form-data" style="display:flex;flex-wrap:wrap;flex-direction:column" method='post'>
            @csrf
            @method('post')

            <div class="form-group row">
                <label for="category_name" class="col-sm-3 col-form-label">Nombre de Categoria</label>
                <input id="category_name" type="text" name="category_name" class="form-control" value="">
                <p id="category_error" ></p>
            </div>

            <button id="submit" type="submit" name="submit" value="submit" class="btn btn-primary" disabled>Agregar</button>
            <p id="submit_error" class="invalid-feedback"></p>
        </form>


        <script>

                let categories = JSON.stringify(@json($categories));
                categories = JSON.parse(categories);
                const categoryName = document.querySelector("#category_name");
                const categoryError = document.querySelector("#category_error");


                categoryName.addEventListener('blur', function(){
                    if(categoryName.value <= 3){
                        categoryError.innerHTML = "Debe ingresar un nombre de categoria correcto";
                        categoryError.style.color = "red";
                        categoryValidation = false;
                        verify()
                    }else{
                        let founds = categories.filter((category) => {
                            return category.name.toLowerCase() === categoryName.value.toLowerCase();
                        });

                        if (founds.length > 0){
                            categoryError.innerHTML = "La categoria ya existe";
                            categoryError.style.color = "red";
                            categoryValidation = false;
                            verify();
                        }else {
                            categoryError.innerHTML = '';
                            categoryValidation = true;
                            verify();
                        }

                    }
                });

            function verify(){
                document.getElementById("submit").disabled = !categoryValidation;
            }

        </script>
    </div>
@endsection
