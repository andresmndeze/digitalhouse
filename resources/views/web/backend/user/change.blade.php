@extends('web.layouts.applr')

@section('title')
  {{"Cambiar configuración del usuario ".$user->email}}
@endsection
@section('content')
	<h1>Cambiar Configuración del Usuario {{$user->email}}</h1>
	<form class="" action="/admin/user/change/" method="post" enctype="multipart/form-data">
		@csrf
			<input type="hidden" name="id" value="{{$user->id}}">
			<label for="name">Nombre</label>
			<input type="text" name="name" value="{{$user->name}}">
			<label for="email">Mail</label>
			<input type="email" name="email" value="{{$user->email}}">
			<label for="is_admin">Opción</label>
			<select name="is_admin">
				<option value="1" @php if ($user->id==1) { echo 'selected';} @endphp">Administrador</option>
				<option value="0" @php if ($user->id==0) { echo 'selected';} @endphp">Usuario</option>
			</select>
      <label id="response" style="width:250px;"></label>
			<button type="submit" name="submit">Editar</button>
	</form>
  <script type="text/javascript" src="/js/edituser.js">

  </script>
@endsection
