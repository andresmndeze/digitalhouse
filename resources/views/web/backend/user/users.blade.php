@extends('web.layouts.applr')
@section('title')
  {{"Users"}}
@endsection
@section('content')

  <h1>Usuarios</h1>

  <div class="table-responsive">
    <table class="table .table-hover table-striped table-bordered">
      <thead class="thead-dark">
        <tr>
          <th scope="col">Nombre</th>
          <th scope="col">Email</th>
          <th scope="col">Tipo</th>
          <th scope="col">Accion</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>
              @if ($user->is_admin == 1)
                Administrador
              @else
                Cliente
              @endif
            </td>
            <td>
              <a class="btn btn-primary" href="/admin/user/{{$user->id}}">
                <span class="fa fa-pencil-alt"></span>
              </a>
              @if ($user->id!=1)
                <form action="/admin/user/delete/{{$user->id}}" method="POST">
                  @csrf
                  @method('post')
                  <button type="submit" class="btn btn-danger">
                    <span class="fa fa-trash"></span>
                  </button>
                </form>
              @endif
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
